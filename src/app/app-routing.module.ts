import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgProgressModule } from "@ngx-progressbar/core";
import { ToastrModule } from "ngx-toastr";

import { HomeComponent } from "./home/home.component";
import { AboutComponent } from './about/about.component';
import { DetailsComponent } from "./details/details.component";
import { GalleryStore } from "./stores/gallery.store";

const routes: Routes = [
  { path: "", pathMatch: "full", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "details/:id", component: DetailsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false }),
    NgbModule,
    NgProgressModule.withConfig({
      spinner: false
    }),
    ToastrModule.forRoot({
      positionClass: "toast-bottom-right",
      preventDuplicates: true
    })
  ],
  exports: [
    RouterModule,
    FormsModule,
    NgbModule,
    NgProgressModule,
    ToastrModule
  ],
  providers: [GalleryStore]
})
export class AppRoutingModule {}
