import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GalleryStore } from "../stores/gallery.store";
import { GalleryItem } from "../models/gallery-item";

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"]
})
export class DetailsComponent implements OnInit {
  galleryItem: GalleryItem;
  itemFound: boolean = false;

  constructor(private route: ActivatedRoute, public store: GalleryStore) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    if (id !== null) {
      let galleryItem = this.store.state.items.find(item => {
        return item.id == id;
      });

      this.itemFound = galleryItem !== undefined;

      if (galleryItem !== undefined) {
        this.galleryItem = galleryItem;
        this.itemFound = true;
      }
    }
  }
}
