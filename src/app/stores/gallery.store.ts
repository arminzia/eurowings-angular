import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";

import { Store } from "./store";
import { GalleryItem } from "../models/gallery-item";
import { GalleryResponse } from "../models/galler-response";

export class GalleryState {
  items: GalleryItem[] = [];
  isLoading: boolean = false;
  error: HttpErrorResponse;
}

@Injectable()
export class GalleryStore extends Store<GalleryState> {
  constructor(private http: HttpClient) {
    super(new GalleryState());
  }

  loadGallery(
    section: string = "hot",
    sort: string = "viral",
    window: string = "day",
    showVirals: boolean = true
  ) {
    this.setState({
      ...this.state,
      isLoading: true
    });

    // api url: https://api.imgur.com/3/gallery/{section}/{sort}/{window}/{page}?showViral=bool
    const apiUrl = `https://api.imgur.com/3/gallery/${section}/${sort}/${window}?showViral=${showVirals}`;

    return this.http.get(apiUrl).pipe(
      tap((response: GalleryResponse) => {
        this.setState({
          ...this.state,
          items: response.data,
          isLoading: false
        });
      }),
      catchError(error => {
        this.setState({
          ...this.state,
          error: error,
          isLoading: false
        });

        return throwError(error);
      })
    );
  }
}
