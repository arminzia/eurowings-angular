import { Component, OnInit } from "@angular/core";
import { GalleryStore } from "../stores/gallery.store";
import { GalleryItem } from "../models/gallery-item";
import { GalleryAlbum } from "../models/gallery-album";
import { GalleryImage } from "../models/gallery-image";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  section: string = "hot";
  sort: string = "viral";
  showVirals: boolean = true;
  window: string = "day";

  constructor(public store: GalleryStore) {}

  ngOnInit() {}

  reload() {
    this.store
      .loadGallery(this.section, this.sort, this.window, this.showVirals)
      .subscribe(
        res => {
          this.store.state.items.forEach((item: GalleryItem) => {
            if (item.is_album == true) {
              const album = item as GalleryAlbum;
              const cover = album.images[0];
              item.media_url = cover.link;
              item.media_type = cover.type;
            } else {
              const image = item as GalleryImage;
              item.media_url = image.link;
              item.media_type = image.type;
            }
          });
        },
        err => {
          console.error(err);
        }
      );
  }
}
