import { GalleryItem } from './gallery-item';

export class GalleryResponse {
  data: GalleryItem[];
  success: boolean;
  status: number;
}