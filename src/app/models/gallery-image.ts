import { GalleryItem } from "./gallery-item";

export class GalleryImage extends GalleryItem {
  type: string;
  animated: boolean;
  width: number;
  height: number;
  size: number;
  bandwidth: number;
  deletehash: string;
  gifv: string;
  mp4: string;
  mp4_size: number;
  looping: boolean;
  section: string;
}
