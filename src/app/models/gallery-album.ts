import { GalleryItem } from "./gallery-item";
import { GalleryImage } from "./gallery-image";

export class GalleryAlbum extends GalleryItem {
  cover: string;
  cover_width: number;
  cover_height: number;
  privacy: string;
  layout: string;
  images_count: number;
  images: GalleryImage[];
}
