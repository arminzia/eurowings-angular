export class GalleryItem {
  id: string;
  title: string;
  description: string;
  datetime: number;
  views: number;
  link: string;
  vote: string;
  favorite: boolean;
  nsfw: boolean;
  comment_count: number;
  topic: string;
  topic_id: number;
  account_url: string;
  account_id: number;
  ups: number;
  downs: number;
  points: number;
  score: number;
  is_album: boolean;
  is_most_viral: boolean;

  // custom props used by this app, for simplicity.
  media_type: string;
  media_url: string;
}
